const path = require('path');
const morgan = require('morgan');
const express = require('express');
const mongoose = require('mongoose');
const app = express();

//configuracion del app o server
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//Haciendo conexión a la bdd mongdb
mongoose.connect('mongodb://localhost/crud1-mongo')
    .then(db=>{console.log('BDD Conectada')})
    .catch(err=>{console.log(err)})

//Importando el enrutador
const indexRoutes= require('./routes/index');

//MidleWare
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));

//Usando las rutas
app.use('/', indexRoutes);


app.listen(app.get('port'), ()=>{
    console.log('Servido corriendo en el puerto:',app.get('port'));
});

