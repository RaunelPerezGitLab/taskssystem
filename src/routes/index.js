const express = require('express');
const Router = express.Router();
const Task = require('../models/task');

Router.get('/', async(req,res)=>{
    const tasks = await Task.find();
    res.render('index',{
        tasks
    });
});

Router.post('/add', async(req, res)=>{
    const task = new Task(req.body);
    await task.save();
    res.redirect('/');
});

Router.get('/delete/:id', async(req,res)=>{
    const { id } = req.params;
    await Task.remove({_id: id});
    res.redirect('/');
});

Router.get('/done/:id', async(req,res)=>{
    const { id } = req.params;
    const task = await Task.findById(id);
    task.status = !task.status;
    await task.save();
    res.redirect('/');
});

Router.get('/edit/:id', async(req,res)=>{
    const { id } = req.params;
    const task = await Task.findById(id);
    res.render('edit', {task});
});

Router.post('/update/:id', async(req,res)=>{
    const { id } = req.params;
    await Task.update({_id: id}, req.body);
    res.redirect('/');
})

module.exports = Router;